ARG BASE=python:3.10.8-slim
FROM ${BASE}
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y --no-install-recommends gcc g++ make python3 python3-dev python3-pip python3-venv python3-wheel espeak-ng libsndfile1-dev && rm -rf /var/lib/apt/lists/*
RUN pip3 install llvmlite --ignore-installed

ENV MODEL_NAME="tts_models/en/vctk/vits"
ENV LIST_MODELS=false

WORKDIR /root
COPY . /root
RUN pip3 install torch torchaudio --extra-index-url https://download.pytorch.org/whl/cu118
RUN make install
CMD python3 TTS/server/server.py --model_name ${MODEL_NAME}
